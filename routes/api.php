<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'client_credential', 'prefix' => 'v1'], function () {


    Route::group(['prefix' => 'user'], function () {
        Route::post('', 'UserController@store');
    });


});


Route::group(['middleware' => 'auth:api', 'prefix' => 'v1'], function () {


    Route::group(['prefix' => 'user'], function () {
        Route::get('', 'UserController@me');
    });


    Route::group(['prefix' => 'post'], function () {
        Route::get('', 'PostController@index');
        Route::post('', 'PostController@store');
        Route::get('{id}', 'PostController@getSinglePost');
    });


    /*Route::group(['prefix' => 'friend'], function () {

    });*/


});


Route::group(['prefix' => 'v1'], function () {


    Route::group(['prefix' => 'notifications'], function () {
        Route::post('', 'NotificationController@sendNotification');
        Route::get('auth/get_password', 'NotificationController@getAuth');
        Route::post('auth/get_password', 'NotificationController@postAuth');
    });

});


Route::group(['prefix' => 'v1'], function () {


    Route::group(['prefix' => 'posts'], function () {
        Route::get('all', 'PostController@allPost');
    });


    /*Route::group(['prefix' => 'friend'], function () {

    });*/


});