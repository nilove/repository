<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        // $this->call(UsersTableSeeder::class);
        factory(App\User::class, 50)->create()->each(function ($u) use ($faker) {
            //dd($u->id);
            \App\Post::create([
                'content' => $faker->sentence(20),
                'user_id' => $u->id,
                'price' => rand(1, 999)
            ]);
        });
        factory(App\Category::class, 50)->create();
        $posts = \App\Post::all();
        $categories = \App\Category::all()->pluck('id')->toArray();
        //dd($categories);
        foreach ($posts as $post) {
            $keys = array_rand($categories, 2);
            $post->categories()->sync([$categories[$keys[0]], $categories[$keys[1]]]);
            \App\Attachment::create(['url' => $faker->imageUrl(400, 500), 'user_id' => $post->user_id, 'post_id' => $post->id,'width'=>400,'height'=>500]);
            \App\Attachment::create(['url' => $faker->imageUrl(600, 400), 'user_id' => $post->user_id, 'post_id' => $post->id,'width'=>600,'height'=>400]);
            \App\Attachment::create(['url' => $faker->imageUrl(400, 400), 'user_id' => $post->user_id, 'post_id' => $post->id,'width'=>400,'height'=>400]);
        }

        $this->seedForExistingUser($faker);
    }


    private function seedForExistingUser($faker)
    {
        $users = \App\User::all();
        foreach ($users as $u) {
            for ($i = 0; $i < 50; $i++) {
                $post = \App\Post::create([
                    'content' => $faker->sentence(20),
                    'user_id' => $u->id,
                    'price' => rand(1, 999)
                ]);

                \App\Attachment::create(['url' => $faker->imageUrl(400, 500), 'user_id' => $post->user_id, 'post_id' => $post->id,'width'=>400,'height'=>500]);
                \App\Attachment::create(['url' => $faker->imageUrl(600, 400), 'user_id' => $post->user_id, 'post_id' => $post->id,'width'=>600,'height'=>400]);
                \App\Attachment::create(['url' => $faker->imageUrl(400, 400), 'user_id' => $post->user_id, 'post_id' => $post->id,'width'=>400,'height'=>400]);
            }

        }
    }
}
