<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'content', 'price', 'user_id'
    ];

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function Categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function Attachments()
    {
        return $this->hasMany(Attachment::class);
    }
}
