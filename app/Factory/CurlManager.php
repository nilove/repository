<?php
namespace App\Factory;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CurlManager
{
    protected $client;
    protected $endpoint;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('api_base_url')]);
    }

    /**
     * Set base url
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->client = new Client(['base_uri' => $url]);
        return $this;
    }

    /**
     *  Process a post request
     * @param $endpoint
     * @param $data
     * @return string
     */
    public function processPostRequest($endpoint, $data = [])
    {
        $response = $this->client->request('POST', $endpoint, ['form_params' => $data]);
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     *  Process a get request
     * @param $endpoint
     * @param $data
     * @return array
     */
    public function processGetRequest($endpoint, $data = [])
    {
        try {

            $res = $this->client->request('GET', $endpoint, ['query' => $data]);
            $response = json_decode($res->getBody()->getContents(), true);
        } catch (ClientException $exception) {
            $responseBody = $exception->getResponse()->getBody()->getContents();
            $response = json_decode($responseBody, true);
        }

        return $response;
    }

    /**
     *  Process a put request
     * @param $endpoint
     * @param $data
     * @return string
     */
    public function processPutRequest($endpoint, $data = [])
    {
        $response = $this->client->request('PUT', $endpoint, ['form_params' => $data]);
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     *  Process a patch request
     * @param $endpoint
     * @param $data
     * @return string
     */
    public function processPatchRequest($endpoint, $data = [])
    {
        $response = $this->client->request('PATCH', $endpoint, ['form_params' => $data]);
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     *  Process a delete request
     * @param $endpoint
     * @param $data
     * @return string
     */
    public function processDeleteRequest($endpoint, $data = [])
    {
        $response = $this->client->request('DELETE', $endpoint, ['form_params' => $data]);
        return json_decode($response->getBody()->getContents(), true);
    }

}