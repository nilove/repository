<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;

class FixTotalImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:totalimages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        Post::with('user', 'attachments')->chunk(20, function ($posts) {
            foreach ($posts  as $post){
                if(!empty($post->toArray()['attachments'])){
                    $post->num_of_attachment = count($post->toArray()['attachments']);
                    $post->save();
                }
            }
        });

    }
}
