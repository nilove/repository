<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 10/6/17
 * Time: 1:58 AM
 */

namespace App\Helper;


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class DiskHelper
{
    public function UploadImageFile($file, $folder)
    {
        $img = Image::make($file->getRealPath())->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
        });
        $file_name = $folder.'/'.time().'.'.$file->getClientOriginalExtension();
        $filename = Storage::disk('public')->put($file_name, $img->stream()->__toString());
        sleep(1);
        return [
            'url' => asset(Storage::url($file_name)),
            'width' => Image::make(Storage::disk('public')->get($file_name))->width(),
            'height' => Image::make(Storage::disk('public')->get($file_name))->height()
        ];
    }


    public function UploadImages($files, $folder)
    {
        $paths = [];
        foreach ($files as $file)
            $paths[] = $this->UploadImageFile($file, $folder);
        return $paths;
    }
}