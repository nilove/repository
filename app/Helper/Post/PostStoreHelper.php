<?php

namespace App\Helper\Post;


use App\Helper\DiskHelper;
use App\Helper\HelperInterface;
use App\Repositories\AttachmentRepository;
use App\Repositories\PostRepository;


class PostStoreHelper implements HelperInterface
{

    private $postRepository, $diskHelper,$attachmentRepository;

    /**
     * PostFactory constructor.
     */
    public function __construct(PostRepository $postRepository,
                                AttachmentRepository $attachmentRepository,
                                DiskHelper $diskHelper)
    {
        $this->postRepository = $postRepository;
        $this->attachmentRepository = $attachmentRepository;
        $this->diskHelper = $diskHelper;
    }


    public function executeHelper($request)
    {
        $inputData = $request->all();
        $user = $request->user();
        $inputData['user_id'] = $user->id;
        $post = $this->postRepository->create($inputData);
        if ($request->file('attachments')) {
            $paths = $this->diskHelper->UploadImages($request->file('attachments'), 'postattachments');
            $this->attachmentRepository->saveAttachments($paths, $post, $user);
        }
        $post->load('user', 'attachments');
        return $post;
    }
}