<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 10/22/17
 * Time: 4:36 AM
 */

namespace App\Helper\Post;


use App\Helper\HelperInterface;
use App\Repositories\Criteria\Post\PostOnlyImages;
use App\Repositories\Criteria\Post\PostOrderByCreatedDesc;
use App\Repositories\Criteria\Post\PostWithRelations;
use App\Repositories\PostRepository;

class PostAllFetchHelper implements HelperInterface
{

    private $postRepository;

    /**
     * PostFetchHelper constructor.
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function executeHelper($request)
    {
        $query = $this->postRepository
            ->getByCriteria(new PostWithRelations(['user', 'attachments']))
            ->getByCriteria(new PostOnlyImages())
            ->getByCriteria(new PostOrderByCreatedDesc());
        return $query->applyCriteria()->paginate(20);
    }
}