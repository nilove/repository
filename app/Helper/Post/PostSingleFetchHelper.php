<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 2/7/18
 * Time: 1:53 AM
 */

namespace App\Helper\Post;


use App\Helper\HelperInterface;
use App\Repositories\Criteria\Post\PostWithRelations;
use App\Repositories\PostRepository;

class PostSingleFetchHelper implements HelperInterface
{

    private $postRepository;

    /**
     * PostFetchHelper constructor.
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function executeHelper($request)
    {

        $query = $this->postRepository
            ->getByCriteria(new PostWithRelations(['user', 'attachments']));
        // ->getByCriteria(new PostById($request));
        return $query->applyCriteria()->find($request);
    }
}