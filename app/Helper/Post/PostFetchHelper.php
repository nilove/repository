<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 10/7/17
 * Time: 2:52 AM
 */

namespace App\Helper\Post;


use App\Helper\HelperInterface;
use App\Repositories\Criteria\Post\PostOrderByCreatedDesc;
use App\Repositories\Criteria\Post\PostsByUser;
use App\Repositories\Criteria\Post\PostWithRelations;
use App\Repositories\PostRepository;

class PostFetchHelper implements HelperInterface
{

    private $postRepository;

    /**
     * PostFetchHelper constructor.
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function executeHelper($request)
    {
        $user = $request->user();
        $query = $this->postRepository
            ->getByCriteria(new PostWithRelations(['user', 'attachments']))
            ->getByCriteria(new PostsByUser($user))
            ->getByCriteria(new PostOrderByCreatedDesc());
        return $query->applyCriteria()->paginate(20);
    }
}