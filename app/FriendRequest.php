<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FriendRequest extends Model
{
    protected $table = 'friend_request';
    protected $fillable = ['user_id1','user_id2','status'];
}
