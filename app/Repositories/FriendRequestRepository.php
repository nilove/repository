<?php
namespace App\Repositories;


class FriendRequestRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\FriendRequest';
    }
}