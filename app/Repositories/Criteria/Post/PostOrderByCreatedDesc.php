<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 10/20/17
 * Time: 2:43 PM
 */

namespace App\Repositories\Criteria\Post;


use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class PostOrderByCreatedDesc extends Criteria
{
    public function apply($model, Repository $repository)
    {
        $query = $model->orderBy("id","desc");
        return $query;
    }
}