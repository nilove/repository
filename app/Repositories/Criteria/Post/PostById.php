<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 2/7/18
 * Time: 11:09 PM
 */

namespace App\Repositories\Criteria\Post;


use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class PostById extends Criteria
{
    private $id;

    /**
     * PostsById constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function apply($model, Repository $repository)
    {
        $query = $model->where('id', $this->id);
        return $query;
    }
}