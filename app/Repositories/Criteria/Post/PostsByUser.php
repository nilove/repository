<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 10/6/17
 * Time: 3:34 AM
 */

namespace App\Repositories\Criteria\Post;


use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class PostsByUser extends Criteria
{
    private $user;

    /**
     * PostsByUser constructor.
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    public function apply($model, Repository $repository)
    {
        $query = $model->where('user_id', $this->user->id);
        return $query;
    }
}