<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 8/15/17
 * Time: 1:14 AM
 */

namespace App\Repositories\Criteria\Post;


use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class SyncCategories extends Criteria
{
    private $categories;

    public function __construct($categories)
    {
        $this->categories = $categories;
    }

    public function apply($model, Repository $repository)
    {
        $query = $model->sync($this->categories);
        return $query;
    }
}