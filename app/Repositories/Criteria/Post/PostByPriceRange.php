<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 8/15/17
 * Time: 12:00 AM
 */

namespace App\Repositories\Criteria\Post;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class PostByPriceRange extends Criteria
{
    private $priceRange;

    public function __construct($priceRange)
    {
        $this->priceRange = $priceRange;
    }

    public function apply($model, Repository $repository)
    {
        $query = $model->whereBetween('price', $this->priceRange);
        return $query;
    }
}