<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 8/15/17
 * Time: 12:00 AM
 */

namespace App\Repositories\Criteria\Post;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class PostByCategories extends Criteria
{
    private $categories;

    public function __construct($categories)
    {
        $this->categories = $categories;
    }

    public function apply($model, Repository $repository)
    {
        $categories = $this->categories;
        $query = $model->whereHas('categories', function ($wherehas) use ($categories) {
            $wherehas->whereIn('id', $categories);
        });
        return $query;
    }
}