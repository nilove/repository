<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 8/15/17
 * Time: 12:00 AM
 */

namespace App\Repositories\Criteria\Post;

use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class PostWithRelations extends Criteria
{
    private $relations;

    public function __construct($relations)
    {
        $this->relations = $relations;
    }

    public function apply($model, Repository $repository)
    {
        $query = $model->with($this->relations);
        return $query;
    }
}