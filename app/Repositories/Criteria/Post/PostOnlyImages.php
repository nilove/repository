<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 10/23/17
 * Time: 6:51 PM
 */

namespace App\Repositories\Criteria\Post;


use App\Repositories\Criteria\Criteria;
use App\Repositories\RepositoryInterface as Repository;

class PostOnlyImages extends Criteria
{


    public function apply($model, Repository $repository)
    {
        $query = $model->where('num_of_attachment','>',0);
        return $query;
    }
}