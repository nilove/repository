<?php
namespace App\Repositories\Criteria;
use App\Repositories\RepositoryInterface as Repository;

/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 8/14/17
 * Time: 6:25 PM
 */
abstract class Criteria
{
    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     */
    public abstract function apply($model, Repository $repository);
}