<?php

namespace App\Repositories;


class AttachmentRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'App\Attachment';
    }


    public function saveAttachments($paths, $post, $user)
    {
        foreach ($paths as $path)
            $this->create(['url' => $path['url'], 'width' => $path['width'], 'height' => $path['height'], 'user_id' => $user->id, 'post_id' => $post->id]);

    }

}