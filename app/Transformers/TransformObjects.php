<?php
namespace App\Transformers;


trait TransformObjects
{
    public function transformUser($item)
    {
        return [
            'user_id' => $item['id'],
            'user_name' => $item['name'],
            'user_email' => $item['email'],
            'profile_pic' => $item['thumb']
        ];
    }


    public function transformAttachments($attachments)
    {
        return array_map([$this, 'transformAttachment'], $attachments);
    }


    public function transformAttachment($attachment){
        return [
            'attachment_id' => $attachment['id'],
            'attachment_url' => $attachment['url'],
            'width' => $attachment['width'],
            'height' => $attachment['height']
        ];
    }
}