<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 10/6/17
 * Time: 1:30 AM
 */

namespace App\Transformers;


class UserTransformer extends Transformer
{
    public function transform($item)
    {
        return $this->transformUser($item);
    }
}