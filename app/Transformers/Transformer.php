<?php

namespace App\Transformers;

abstract class Transformer
{
    use TransformObjects;

    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    public abstract function transform($item);
}