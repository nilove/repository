<?php

namespace App\Transformers;


class PostTransformer extends Transformer
{
    public function transform($item)
    {
        return [
            'post_id' => $item['id'],
            'content' => $item['content'],
            'price' => $item['price'],
            'user' => $this->transformUser($item['user']),
            'attachments' => $this->transformAttachments($item['attachments']),
            'created' => strtotime($item['created_at'])
        ];
    }
}