<?php

namespace App\Http\Controllers;

use App\Helper\DiskHelper;
use App\Repositories\UserRepository as User;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class UserController extends ApiController
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UserTransformer $userTransformer, DiskHelper $diskHelper)
    {
        $inputData = $request->all();
        $validator = $this->ValidateRequest($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:3',
            'thumb' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if ($validator->fails()) return $this->respondValidationError('Fields Validation Failed.', $validator->errors());
        $inputData['thumb'] = $diskHelper->UploadImageFile($request->file('thumb'), 'profiles')['url'];
        $inputData['password'] = bcrypt($inputData['password']);
        $user = $this->user->create($inputData);
        return $this->respondCreated("User Created Successfully", $userTransformer->transform($user->toArray()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function me(Request $request, UserTransformer $userTransformer)
    {
        return $userTransformer->transform($request->user()->toArray());
    }
}
