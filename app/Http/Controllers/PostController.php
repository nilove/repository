<?php

namespace App\Http\Controllers;


use App\Helper\Post\PostAllFetchHelper;
use App\Helper\Post\PostFetchHelper;
use App\Helper\Post\PostSingleFetchHelper;
use App\Helper\Post\PostStoreHelper;
use App\Transformers\PostTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends ApiController
{

    /**
     * Display a listing of the resource.
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, PostFetchHelper $postFetchHelper, PostTransformer $postTransformer)
    {
        try {
            $posts = $postFetchHelper->executeHelper($request);
            return $this->respondWithPagination($posts, $postTransformer->transformCollection($posts->toArray()['data']), 'Success');
        } catch (\Exception $e) {

            return $this->respondInternalError('Error in processing request');
        }

    }



    /**
     * Store a newly created resource in storage.
     * @param $request
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PostTransformer $postTransformer, PostStoreHelper $postStoreHelper)
    {

        try {
            $validator = $this->ValidateRequest($request, [
                'content' => 'required|max:255',
                'attachments' => 'array:between:1,4',
                'attachments.0' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
                'attachments.1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192',
                'attachments.2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:8192'
            ]);
            if ($validator->fails()) return $this->respondValidationError('Fields Validation Failed.', $validator->errors());
            DB::beginTransaction();
            $post = $postStoreHelper->executeHelper($request);
            DB::commit();
            return $this->respondCreated("Post Created Successfully", $postTransformer->transform($post->toArray()));
        } catch (\Exception $e) {
            DB::rollBack();
            return \Response::json(['error' => 'Error in processing']);
        }

    }


    /**
     * Display a listing of the resource.
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function allPost(Request $request, PostAllFetchHelper $postFetchHelper, PostTransformer $postTransformer)
    {
        try {
            $posts = $postFetchHelper->executeHelper($request);
            return $this->respondWithPagination($posts, $postTransformer->transformCollection($posts->toArray()['data']), 'Success');
        } catch (\Exception $e) {
            return $this->respondInternalError('Error in processing request');
        }

    }


    public function getSinglePost(PostSingleFetchHelper $postSingleFetchHelper, PostTransformer $postTransformer, $id)
    {
        try {
            $post = $postSingleFetchHelper->executeHelper($id);
            return ($post ? $this->respond($postTransformer->transform($post->toArray())) : $this->respondNotFound('Post not found'));
        } catch (\Exception $e) {
            return $this->respondInternalError('Error in processing request');
        }
    }


}
