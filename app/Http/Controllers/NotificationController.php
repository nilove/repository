<?php
/**
 * Created by PhpStorm.
 * User: kausersarker
 * Date: 10/17/17
 * Time: 5:49 PM
 */

namespace App\Http\Controllers;


use App\PushNotification\Firebase;
use App\PushNotification\Push;
use Illuminate\Http\Request;

class NotificationController extends ApiController
{


    protected $firebase, $push;


    /**
     * NotificationController constructor.
     */
    public function __construct(Firebase $firebase, Push $push)
    {
        $this->firebase = $firebase;
        $this->push = $push;
    }


    public function sendNotification(Request $request)
    {
        try {
            $input = $request->all();
            // optional payload
            $payload = array();
            $payload['team'] = 'Bangladesh';
            $payload['score'] = '5.6';

            // notification title
            $title = isset($input['title']) ? $input['title'] : '';

            // notification message
            $message = isset($input['message']) ? $input['message'] : '';

            // push type - single user / topic
            $push_type = isset($input['push_type']) ? $input['push_type'] : '';

            // whether to include to image or not
            $include_image = isset($input['include_image']) ? TRUE : FALSE;


            $this->push->setTitle($title);
            $this->push->setMessage($message);
            if ($include_image) {
                $this->push->setImage('https://api.androidhive.info/images/minion.jpg');
            } else {
                $this->push->setImage('');
            }
            $this->push->setIsBackground(FALSE);
            $this->push->setPayload($payload);


            $json = '';
            $response = '';

            if ($push_type == 'topic') {
                $json = $this->push->getPush();
                // dd($json);
                $response = $this->firebase->sendToTopic('global', $json);
            } else if ($push_type == 'individual') {
                $json = $this->push->getPush();
                $regId = isset($input['regId']) ? $input['regId'] : '';
                $response = $this->firebase->send($regId, $json);
            }

            return $this->respondCreated("Notification Send successfully", []);
        } catch (\Exception $e) {

            return \Response::json(['error' => 'Error in processing']);
        }
    }


    public function getAuth(Request $request)
    {
        \Log::info(json_encode($request->all()));
        return "c0mm0n";
        return \Response::json(['response' => ['message' => 'Verified profile', 'status' => 200]], 200);
    }

    public function postAuth(Request $request)
    {
        \Log::info(json_encode($request->all()));
        return \Response::json(['response' => ['message' => 'Verified profile', 'status' => 200]], 200);
    }
}